<?php

use yii\db\Migration;

class m160520_172759_AddColumnChifInUser extends Migration
{
    protected $tableName='users';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'chief', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'chief');
    }

}
