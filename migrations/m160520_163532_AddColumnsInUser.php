<?php

use yii\db\Migration;

class m160520_163532_AddColumnsInUser extends Migration
{
    protected $tableName='users';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'inactive', $this->boolean());
        $this->addColumn($this->tableName, 'role', $this->integer());
        $this->addColumn($this->tableName, 'place', $this->integer());
        $this->addColumn($this->tableName, 'spec', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'inactive');
        $this->dropColumn($this->tableName, 'role');
        $this->dropColumn($this->tableName, 'place');
        $this->dropColumn($this->tableName, 'spec');
    }

}
