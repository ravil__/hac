<?php

use yii\db\Migration;

class m160506_075012_AddUserTable extends Migration
{
    public function up()
    {
        // создаем таблицу
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'objectsid' => $this->string(100)->unique()->notNull(),
            'cn' => $this->string(255),
            'mail' => $this->string(100),
            'dn' => $this->string(255),
            'sn' => $this->string(255),
            'displayname' => $this->string(255),
            'title' => $this->string(255),
            'objectguid' => $this->string(255),
            'password' => $this->string(60),
            'auth_key' => $this->string(32)->notNull(),
            'isadmin' => $this->boolean()->notNull()->defaultValue(false),
            'thumbnailphoto' => $this->text(),
        ]);

    }

    public function down()
    {
        echo "m160506_075012_AddUserTable cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
