<?php

use yii\db\Migration;

class m160524_161059_AddColumnsToUsers extends Migration
{
    protected $tableName='users';

    public function safeUp()
    {
        $this->addColumn($this->tableName, "tags_ids", $this->string() );
        $this->addColumn($this->tableName, "changed_by_chief", $this->boolean() );
        $this->addColumn($this->tableName, "place_id", $this->integer() );
        $this->addColumn($this->tableName, "chief_id", $this->integer() );
        $this->addColumn($this->tableName, "spec_id", $this->integer() );
        $this->addColumn($this->tableName, "role_id", $this->integer() );
        $this->dropColumn($this->tableName, "place" );
        $this->dropColumn($this->tableName, "spec" );
        $this->dropColumn($this->tableName, "chief" );
        $this->dropColumn($this->tableName, "role" );

    }

    public function saveDown()
    {
        $this->addColumn($this->tableName, "place", $this->integer() );
        $this->addColumn($this->tableName, "chief", $this->integer() );
        $this->addColumn($this->tableName, "spec", $this->integer() );
        $this->addColumn($this->tableName, "role", $this->integer() );
        $this->dropColumn($this->tableName, "place_id" );
        $this->dropColumn($this->tableName, "spec_id" );
        $this->dropColumn($this->tableName, "chief_id" );
        $this->dropColumn($this->tableName, "role_id" );
        $this->dropColumn($this->tableName, "tags_ids" );
        $this->dropColumn($this->tableName, "changed_by_chief" );

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
