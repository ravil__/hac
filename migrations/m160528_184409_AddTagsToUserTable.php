<?php

use yii\db\Migration;

class m160528_184409_AddTagsToUserTable extends Migration
{
    public function up()
    {
        $this->createTable('tag_to_user',[
            'id' => $this->primaryKey(),
            'iser_id' => $this->integer(),
            'tag_id' =>$this->integer(),
        ]);
    }

    public function down()
    {
        $this->dropTable('tag_to_user');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
