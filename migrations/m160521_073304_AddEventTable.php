<?php

use yii\db\Migration;

class m160521_073304_AddEventTable extends Migration
{
    public function up()
    {
        $this->createTable('event',[
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'creator' => $this->integer()->notNull(),
            'start_datetime' => $this->dateTime(),
            'end_datetime' => $this->dateTime(),
            'category' => $this->integer(),
            'place' => $this->integer(),
            'approve_to_go' => $this->boolean()->notNull()->defaultValue(false),
            'active' => $this->boolean()->notNull()->defaultValue(true),
            'approve_before' => $this->dateTime(),
            'approve_role' => $this->integer(),
            'json' => $this->text(),
            'desc' => $this->text(),
        ]);

    }

    public function down()
    {
        $this->dropTable("event");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
