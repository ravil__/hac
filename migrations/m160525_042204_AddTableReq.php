<?php

use yii\db\Migration;

class m160525_042204_AddTableReq extends Migration
{
    public function up()
    {
        $this->createTable('req',[
            'id' => $this->primaryKey(),
            'member_id' => $this->integer()->notNull(),
            'event_id' => $this->integer()->notNull(),
            'weight' => $this->string(),
            'free' => $this->boolean(),
            'approved_by_leed' => $this->boolean(),
            'approved_by_pm' => $this->boolean(),
            'approved_by_boss' => $this->boolean(),
            'comment' => $this->text(),
            'del_time' => $this->dateTime(),
            'create_time'  => $this->dateTime(),
        ]);

    }

    public function down()
    {
        $this->dropTable("req");
    }
}
