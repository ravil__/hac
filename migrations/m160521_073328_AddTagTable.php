<?php

use yii\db\Migration;

class m160521_073328_AddTagTable extends Migration
{
    public function up()
    {
        $this->createTable('tag',[
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);
    }

    public function down()
    {
        $this->dropTable("tag");
    }
}
