<?php

use yii\db\Migration;

class m160525_004709_ChancheColumnsInEvent extends Migration
{
    protected $tableName='event';

    public function safeUp()
    {
        $this->addColumn($this->tableName, "tags_ids", $this->string() );

        $this->addColumn($this->tableName, "place_id", $this->integer() );
        $this->addColumn($this->tableName, "approve_role_id", $this->integer() );
        $this->addColumn($this->tableName, "creator_id", $this->integer() );
        $this->addColumn($this->tableName, "category_id", $this->integer() );

        $this->dropColumn($this->tableName, "place" );
        $this->dropColumn($this->tableName, "category" );
        $this->dropColumn($this->tableName, "creator" );
        $this->dropColumn($this->tableName, "approve_role" );

    }

    public function saveDown()
    {
        $this->addColumn($this->tableName, "place", $this->integer() );
        $this->addColumn($this->tableName, "approve_role", $this->integer() );
        $this->addColumn($this->tableName, "category", $this->integer() );
        $this->addColumn($this->tableName, "creator", $this->integer() );

        $this->dropColumn($this->tableName, "place_id" );
        $this->dropColumn($this->tableName, "creator_id" );
        $this->dropColumn($this->tableName, "approve_role_id" );
        $this->dropColumn($this->tableName, "category_id" );

        $this->dropColumn($this->tableName, "tags_ids" );

    }
}
