<?php

use yii\db\Migration;

class m160521_091349_AddColumnCreationDatetime extends Migration
{
    protected $tableName='event';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'creation_datetime', $this->dateTime());
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'creation_datetime');
    }

}
