<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "req".
 *
 * @property integer $id
 * @property integer $member_id
 * @property integer $event_id
 * @property string $weight
 * @property boolean $free
 * @property boolean $approved_by_leed
 * @property boolean $approved_by_pm
 * @property boolean $approved_by_boss
 * @property string $comment
 * @property string $del_time
 * @property string $create_time
 */
class Req extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'req';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'event_id'], 'required'],
            [['member_id', 'event_id'], 'integer'],
            [['free', 'approved_by_leed', 'approved_by_pm', 'approved_by_boss'], 'boolean'],
            [['comment'], 'string'],
            [['del_time', 'create_time'], 'safe'],
            [['weight'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'event_id' => 'Event ID',
            'weight' => 'Weight',
            'free' => 'Free',
            'approved_by_leed' => 'Approved By Leed',
            'approved_by_pm' => 'Approved By Pm',
            'approved_by_boss' => 'Approved By Boss',
            'comment' => 'Comment',
            'del_time' => 'Del Time',
            'create_time' => 'Create Time',
        ];
    }
}
