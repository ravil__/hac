<?php


namespace app\models;
use yii\filters\auth\AuthMethod;

class HeaderParamsAuth extends AuthMethod
{
    /**
     * @var string the parameter name for passing the access token
     */
    public $tokenParam = 'X-Token';


    /**
     * @inheritdoc
     */
    public function authenticate($user, $request, $response)
    {
        $accessTokens = $request->getHeaders();
        $accessToken = $accessTokens->get($this->tokenParam);

        if (is_string($accessToken)) {
            $identity = $user->loginByAccessToken($accessToken, get_class($this));
            if ($identity !== null) {
                return $identity;
            }
        }
        if ($accessToken !== null) {
            $this->handleFailure($response);
        }

        return null;
    }
}
