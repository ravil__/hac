<?php
/**
 * Created by PhpStorm.
 * User: ravil
 * Date: 5/20/16
 * Time: 9:12 PM
 */

namespace app\models;


class Spec {

    private $ref = [
        ['id' => 1, 'name' => 'Андроид разработка'],
        ['id' => 2, 'name' => 'PHP разработка'],
        ['id' => 3, 'name' => 'iOS разработка']
    ];

    public function  findAll() {
        return $this->ref;
    }

}