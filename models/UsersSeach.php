<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Users;

/**
 * UsersSeach represents the model behind the search form about `app\models\Users`.
 */
class UsersSeach extends Users
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['objectsid', 'cn', 'mail', 'dn', 'sn', 'displayname', 'title', 'objectguid', 'password', 'auth_key', 'thumbnailphoto'], 'safe'],
            [['isadmin'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Users::find()->where(['inactive' => false]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'isadmin' => $this->isadmin,
        ]);

        $query->andFilterWhere(['like', 'objectsid', $this->objectsid])
            ->andFilterWhere(['like', 'cn', $this->cn])
            ->andFilterWhere(['like', 'mail', $this->mail])
            ->andFilterWhere(['like', 'dn', $this->dn])
            ->andFilterWhere(['like', 'sn', $this->sn])
            ->andFilterWhere(['like', 'displayname', $this->displayname])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'objectguid', $this->objectguid])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'thumbnailphoto', $this->thumbnailphoto]);

        return $dataProvider;
    }
}
