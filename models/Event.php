<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event".
 *
 * @property integer $id
 * @property string $name
 * @property integer $creator
 * @property string $start_datetime
 * @property string $end_datetime
 * @property integer $category
 * @property integer $place
 * @property boolean $approve_to_go
 * @property boolean $active
 * @property string $approve_before
 * @property string $creation_datetime
 * @property string $json
 * @property string $desc
 */
class Event extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['creator_id', 'category_id', 'place_id', 'approve_role_id'], 'integer'],
            [['start_datetime', 'end_datetime', 'approve_before', 'creation_datetime' ], 'safe'],
            [['approve_to_go', 'active'], 'boolean'],
            //[['json', 'desc'], 'string'],
            [['desc'], 'string'],
            [['name','tags_ids'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'creator' => 'Creator',
            'start_datetime' => 'Start Datetime',
            'end_datetime' => 'End Datetime',
            'category' => 'Category',
            'place' => 'Place',
            'approve_to_go' => 'Approve To Go',
            'active' => 'Active',
            'approve_before' => 'Approve Before',
            'approve_role' => 'Approve Role',
            'json' => 'Json',
            'desc' => 'Desc',
            'creation_datetime' => 'Creation Datetime'
        ];
    }


    public function extraFields()
    {
        return ['requesttotalcount', 'requests'];
    }

    public function getRequests()
    {
        return $this->HasMany(Req::className(), ['event_id' => 'id']);
    }

    public function getRequesttotalcount()
    {
        return $this->HasMany(Req::className(), ['event_id' => 'id'])->count();
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave ($insert) {
        if ($insert) {
            // пользовательский айди добавить
            $this->creator_id = Yii::$app->user->id;
            // обнуляем что не должно быть
            $this->approve_before = null;
            $this->approve_role_id = null;
            // первоначально показываем ли
            $this->active = ($this->approve_to_go === false)? true: false;
            // напишем дату создания
            $date = new \DateTime();
            $this->creation_datetime = $date->format("Y-m-d H:i:s");
        }
        //$this->uptime = time();

        $dataIn = Yii::$app->getRequest()->getBodyParams();
        $this->json = json_encode($dataIn['json']);

        return parent::beforeSave($insert);
    }
}
