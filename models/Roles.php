<?php
/**
 * Created by PhpStorm.
 * User: ravil
 * Date: 5/20/16
 * Time: 9:12 PM
 */

namespace app\models;


class Roles {

    private $ref = [
        ['id' => 4, 'name' => 'Руководитель проекта обучения'],
        ['id' => 3, 'name' => 'Проект-Менеджер'],
        ['id' => 2, 'name' => 'Тимлид'],
        ['id' => 1, 'name' => 'Участник']
    ];

    public static function getApproveEventRoleId ()  {
        return 3;
    }

    /**
     * @param $role_id
     * @return bool
     */
    public static function mayHaveChildren ($role_id) {
        if ($role_id == 1) {
            return false;
        }
        return true;
    }

    public static function prevAproove ($role_id) {
        if ($role_id == 2 || $role_id == 1) {
            return null;
        }elseif($role_id == 3 ){
            return "approved_by_leed";
        }elseif($role_id == 4) {
            return "approved_by_pm";
        }

    }

    public static function globalChangeUsersRole () {
        return [3];
    }

    public function  findAll() {
        return $this->ref;
    }

}