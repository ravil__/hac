<?php
/**
 * Created by PhpStorm.
 * User: ravil
 * Date: 6/12/16
 * Time: 4:57 PM
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\rest\Action;
use yii\db\ActiveRecord;
use yii\web\ServerErrorHttpException;

class MemberUpdateAction extends Action {
    /**
     * @var string the scenario to be assigned to the model before it is validated and updated.
     */
    public $scenario = Model::SCENARIO_DEFAULT;


    /**
     * Updates an existing model.
     * @param string $id the primary key of the model.
     * @return \yii\db\ActiveRecordInterface the model being updated
     * @throws ServerErrorHttpException if there is any error when updating the model
     */
    public function run($id)
    {
        /* @var $model ActiveRecord */
        $model = $this->findModel($id);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }

        $model->scenario = $this->scenario;


        // загрузить в модельку только нужные мне параметры а не все
        $inData = Yii::$app->getRequest()->getBodyParams();


        $data['place_id']= (isset($inData['place_id']) ) ? $inData['place_id']:null;
        $data['spec_id'] = (isset($inData['spec_id']) ) ? $inData['spec_id']:null;
        if (isset($inData['chief_id']) && $chief = Users::findOne($inData['chief_id'])) {
               $data['chief_id'] = $chief->id;
        }


        $data['tags_ids'] = (isset($inData['tags_ids']) ) ? $inData['tags_ids']:null;

        $model->load($data, '');


        if ($model->save() === false && !$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
        }

        return $model;
    }
}