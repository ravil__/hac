<?php
/**
 * Created by PhpStorm.
 * User: ravil
 * Date: 5/20/16
 * Time: 9:12 PM
 */

namespace app\models;


class Category {

    private $ref = [
        ['id' => 1, 'name' => 'Бэкенд'],
        ['id' => 2, 'name' => 'Фронтенд'],
        ['id' => 3, 'name' => 'Ангуляр'],
    ];

    public function  findAll() {
        return $this->ref;
    }

}