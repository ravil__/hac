<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $objectsid
 * @property string $cn
 * @property string $mail
 * @property string $dn
 * @property string $sn
 * @property string $displayname
 * @property string $title
 * @property string $objectguid
 * @property string $password
 * @property string $auth_key
 * @property boolean $isadmin
 * @property string $thumbnailphoto
 * @property boolean $inactive
 * @property integer $role_id
 * @property integer $place_id
 * @property integer $spec_id
 * @property string $tags_ids
 */
class Users extends \yii\db\ActiveRecord  implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['objectsid', 'auth_key'], 'required'],
            [['isadmin', 'inactive'], 'boolean'],
            [['role_id', 'place_id', 'spec_id', 'chief_id'], 'integer'],
            [['thumbnailphoto' ], 'string'],
            [['objectsid', 'mail'], 'string', 'max' => 100],
            [['cn', 'dn', 'sn', 'displayname', 'title', 'objectguid', 'tags_ids'], 'string', 'max' => 255],
            [['password'], 'string', 'max' => 60],
            [['auth_key'], 'string', 'max' => 32],
            [['objectsid'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'objectsid' => 'Objectsid',
            'cn' => 'Cn',
            'mail' => 'Mail',
            'dn' => 'Dn',
            'sn' => 'Sn',
            'displayname' => 'Displayname',
            'title' => 'Title',
            'objectguid' => 'Objectguid',
            'password' => 'Password',
            'auth_key' => 'Auth Key',
            'isadmin' => 'Isadmin',
            'thumbnailphoto' => 'Thumbnailphoto',
            'inactive' => 'Inactive',
            'role_id' => 'Role',
            'place_id' => 'Place',
            'spec_ids' => 'Spec',
        ];
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['sn' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }

    public function login () {

    }

    /**
     *
     * @param bool $insert
     */
    public function afterSave($insert, $changedAttributes)
    {
        // здесь будет логика пересохранения тэгов пока ориентируемся на json поле
        if (
            isset($changedAttributes['tags_ids'])
        ||
            json_decode($this->tags_ids) && ($changedAttributes['tags_ids'] === null)
        ) {
            $tagsToSaveArr = (isset($this->tags_ids)) ? json_decode($this->tags_ids) : [];
            $existTags = json_decode($changedAttributes['tags_ids']);
            $tagsToSaveArr = (is_array($tagsToSaveArr)) ? $tagsToSaveArr : [];
            $existTags = (is_array($existTags)) ? $existTags : [];

            foreach ($tagsToSaveArr as $tagToSave) {
                if (!in_array($tagToSave, $existTags)) {
                    $tag = new TagToUser();
                    $tag->iser_id = Yii::$app->user->id;
                    $tag->tag_id = $tagToSave;
                    $tag->save();
                }
            }

            foreach ($existTags as $tagToDel) {
                if (!in_array($tagToDel, $tagsToSaveArr)) {
                    $tag = TagToUser::findOne(['iser_id' => Yii::$app->user->id, 'tag_id' => $tagToDel]);
                    if ($tag) {
                        $tag->delete();
                    }
                }
            }
        }
        // шлем сообщение на событие
        MailEvents::addEvent("SendUserUpdate", $changedAttributes);
    }

    public function extraFields()
    {
        return ['taglist', 'requests'];
    }

    public function getRequests()
    {
        return $this->HasMany(Req::className(), ['member_id' => 'id']);
    }

    public function getTaglist()
    {
        return $this->HasMany(Tag::className(), ['id' => 'tag_id'])
            ->viaTable('tag_to_user',['iser_id' => 'id']);
    }



}
