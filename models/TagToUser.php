<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tag_to_user".
 *
 * @property integer $id
 * @property integer $iser_id
 * @property integer $tag_id
 *
 * @property Tag $tag
 * @property Users $user
 */
class TagToUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tag_to_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iser_id', 'tag_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'iser_id' => 'Iser ID',
            'tag_id' => 'Tag ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'iser_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(Tag::className(), ['id' => 'tag_id']);
    }
}
