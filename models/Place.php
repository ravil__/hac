<?php
/**
 * Created by PhpStorm.
 * User: ravil
 * Date: 5/20/16
 * Time: 9:12 PM
 */

namespace app\models;


class Place {

    private $ref = [
        ['id' => 1, 'name' => 'Самара, Галактика'],
        ['id' => 2, 'name' => 'Самара БК'],
        ['id' => 3, 'name' => 'Тольятти'],
    ];

    public function  findAll() {
        return $this->ref;
    }

}