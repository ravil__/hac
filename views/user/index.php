<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel  app\models\UsersSeach */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Users', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'objectsid',
            'cn',
            'mail',
            'dn',
            'sn',
            'displayname',
            'title',
            // 'objectguid',
            // 'password',
            // 'auth_key',
            // 'isadmin:boolean',
            [
                'attribute'=>'thumbnailphoto',
                'contentOptions' =>['class' => 'table_class','style'=>'display:block;'],
                'content'=>function($data){
                    if ($data->thumbnailphoto) {
                        $res = "<img src='data:image/jpg;base64,".(string)$data->thumbnailphoto."'></a>";
                    }else{
                        $res = "Нет фото";
                    }
                    return $res;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
