<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UsersSeach */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'objectsid') ?>

    <?= $form->field($model, 'cn') ?>

    <?= $form->field($model, 'mail') ?>

    <?= $form->field($model, 'dn') ?>

    <?php // echo $form->field($model, 'sn') ?>

    <?php // echo $form->field($model, 'displayname') ?>

    <?php // echo $form->field($model, 'title') ?>

    <?php // echo $form->field($model, 'objectguid') ?>

    <?php // echo $form->field($model, 'password') ?>

    <?php // echo $form->field($model, 'auth_key') ?>

    <?php // echo $form->field($model, 'isadmin')->checkbox() ?>

    <?php // echo $form->field($model, 'thumbnailphoto') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
