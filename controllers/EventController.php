<?php

namespace app\controllers;

use app\models\Event;
use app\models\HeaderParamsAuth;
use app\models\Roles;
use Yii;
use app\models\Users;
use app\models\UsersSeach;
use yii\base\UserException;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\User;


class EventController extends BaseActiveController
{
    public $modelClass = 'app\models\Event';

    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function prepareDataProvider () {

        $dataIn = Yii::$app->getRequest()->get();

        $query =  Event::find();
        // добавить по умолиииию связь тэги и посмоььььь как будет филооо отрабатывать


        // по умолчанию только активные
        if (Roles::getApproveEventRoleId() !=  Yii::$app->user->id) {
            $query = $query->where(['active' => true]);
        }


        if (isset($dataIn['name'])) {
            $query = $query->andFilterWhere(['like', 'name', $dataIn['name']]);
        }

        if (isset($dataIn['creator_ids'])) {
            $idArray = explode(",",$dataIn['creator_ids']);
            $query = $query->andFilterWhere(['creator_id' => $idArray]);
        }

        if (isset($dataIn['category_ids'])) {
            $idArray = explode(",",$dataIn['category_ids']);
            $query = $query->andFilterWhere(['category_id' => $idArray]);
        }

        if (isset($dataIn['place_ids'])) {
            $idArray = explode(",",$dataIn['place_ids']);
            $query = $query->andFilterWhere(['place_id' => $idArray]);
        }

        if (isset($dataIn['ids'])) {
            $idArray = explode(",",$dataIn['ids']);
            $query = $query->andFilterWhere(['id' => $idArray]);
        }

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }

    public function checkAccess($action, $model = null, $params = [])
    {

        if ($action == "delete"  ) {
            throw new ForbiddenHttpException("Пока никто не может удалять мероприятие");
        }

        if ($action == "update") {

            $creator = Users::findOne($model->creator_id);

            $canIChange = true;//( $creator->id == Yii::$app->user->id );
            $canLeedChange = ($creator->chief_id == Yii::$app->user->id);
            $leed = Users::findOne($creator->chief_id);
            $canPMChange = ($leed)? ($leed->chief_id == Yii::$app->user->id) : false;
            $isBossRole = in_array(Yii::$app->user->id,Roles::globalChangeUsersRole()) ;

            if (!($canIChange || $canLeedChange || $canPMChange || $isBossRole)) {
                throw new ForbiddenHttpException("Вы не можете редактировать это событие");
            }
        }
    }

}
