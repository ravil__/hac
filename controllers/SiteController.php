<?php

namespace app\controllers;

use app\models\Users;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }


    public function actionInfo ($pass="OOz852rYm") {
//        iconv_set_encoding("internal_encoding", "UTF-8");
//        iconv_set_encoding("output_encoding", "UTF-8");
//        iconv_set_encoding("input_encoding", "UTF-8");

        $ds=ldap_connect("172.29.134.240");
        ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
        if ($ds) {

            $r=ldap_bind($ds,"alt\\ravil.shamenov",$pass);

            $dn="OU=HQ,DC=alt,DC=altarix,DC=ru";
            $sr = ldap_search($ds, $dn, "(objectClass=user)", array('distinguishedname','department', 'displayname', 'cn', 'sn','title','company','mail','jpegphoto','thumbnailphoto','objectguid','objectsid'));

            //toDo поправить импорт контактов
            $info = ldap_get_entries($ds, $sr);

            $out =array();
            for ($i=0; $i<$info["count"]; $i++) {
                $out[] = array(
                    "cn" => (isset($info[$i]['cn']))? iconv ( "Windows-1251" , "UTF-8", $info[$i]['cn']["0"]):null,
                    "distinguishedname" => (isset($info[$i]['distinguishedname']))?iconv ( "Windows-1251" , "UTF-8",$info[$i]['distinguishedname']["0"]):null,
                    "department" => (isset($info[$i]['department']))? iconv ( "Windows-1251" , "UTF-8",$info[$i]['department']["0"]):null,
                    "displayname" => (isset($info[$i]['displayname']))? iconv ( "Windows-1251" , "UTF-8",$info[$i]['displayname']["0"]):null,
                    "dn" => (isset($info[$i]['dn']))? iconv ( "Windows-1251" , "UTF-8", $info[$i]['dn']):null,
                    "sn" => (isset($info[$i]['sn']))? iconv ( "Windows-1251" , "UTF-8", $info[$i]['sn']["0"]):null,
                    "title" => (isset($info[$i]['title']))? iconv ( "Windows-1251" , "UTF-8", $info[$i]['title']["0"]):null,
                    "mail" => (isset($info[$i]['mail']))? iconv ( "Windows-1251" , "UTF-8", $info[$i]['mail']["0"]):null,
                    "thumbnailphoto" => (isset($info[$i]['thumbnailphoto']))? base64_encode($info[$i]['thumbnailphoto']["0"] ):null,
                    "jpegphoto" => (isset($info[$i]['jpegphoto']))? base64_encode($info[$i]['jpegphoto']["0"] ):null,
                    "objectsid" => (isset($info[$i]['objectsid']))? base64_encode($info[$i]['objectsid']["0"] ):null,
                    "objectguid" => (isset($info[$i]['objectguid']))? base64_encode($info[$i]['objectguid']["0"] ):null,
                );
            }

            ldap_close($ds);

            foreach ($out as $userData) {
                $user = new Users();
                $user->cn = $userData['cn'];
                $user->objectsid = $userData['objectsid'];
                $user->mail = $userData['mail'];
                $user->dn = $userData['dn'];
                $user->sn = $userData['sn'];
                $user->displayname = $userData['displayname'];
                $user->title = $userData['title'];
                $user->objectguid = $userData['objectguid'];
                $user->auth_key = Yii::$app->security->generateRandomString();
                $user->isadmin = false;
                $user->thumbnailphoto = $userData['thumbnailphoto'];
                $user->inactive = (strpos( $userData['dn'] , "Отключенные_пользователи") === false)? false : true;

                $result = $user->save();
                if (!$result){
                    throw new UserException(array_shift($user->getFirstErrors()));
                }
            }

            return $this->render('info', [
                'info' => "Ок",
            ]);

        } else {
            throw new UserException ("Невозможно подключиться к серверу LDAP");
        }
    }
}
