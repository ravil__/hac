<?php

namespace app\controllers;


use app\models\Req;
use app\models\Roles;
use Yii;
use app\models\Users;
use app\models\UsersSeach;
use yii\base\UserException;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\User;


class ReqController extends BaseActiveController
{
    public $modelClass = 'app\models\Req';

    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        return $actions;
    }

    protected function findChildren ($id) {
        $users = Users::find()->where(['chief' => $id]);
        $out = [];
        foreach ($users as $user) {
            if (Roles::mayHaveChildren($user->role_id)) {
                $out = array_merge($out, $this->findChildren($user->id));
            }else{
                $out[] = $user->id;
            }
        }
        return $out;
    }

    public function prepareDataProvider () {

        $dataIn = Yii::$app->getRequest()->get();

        $id = Yii::$app->user->id;

        if (! isset($dataIn['not_mine']) || $dataIn['not_mine'] == "false") {
            // свои
            $query =  Req::find()->where(['member_id' => $id]);// todo по времени удаления ->where(['del_time' => false]);
        }else{
            // на утверждение не мои
            $role = Yii::$app->user->identity->role_id;
            // сначала найдем айдишники всех подчиненных пользователей
            $ids = $this->findChildren($id);
            $query =  Req::find()->where(['member_id' => $ids]);
            // потом посмотрим утверждены ли они на предыдущем уровне
            $column = Roles::prevAproove($role);
            if ($column !== null) {
                $query = $query->andFilterWhere([$column => null]);
            }

        }

        if (isset($dataIn['ids'])) {
            $idArray = explode(",",$dataIn['ids']);
            $query = $query->andFilterWhere(['id' => $idArray]);
        }


        if (isset($dataIn['event_ids'])) {
            $idArray = explode(",",$dataIn['event_ids']);
            $query = $query->andFilterWhere(['event_id' => $idArray]);
        }

        if (isset($dataIn['member_ids'])) {
            $idArray = explode(",",$dataIn['member_ids']);
            $query = $query->andFilterWhere(['event_id' => $idArray]);
        }

        if (isset($dataIn['free'])) {
            $free = filter_var($dataIn['free'], FILTER_VALIDATE_BOOLEAN);
            $query = $query->andFilterWhere(['free' => $free]);
        }

        if (isset($dataIn['approved_by_leed'])) {
            $approved_by_leed = filter_var($dataIn['approved_by_leed'], FILTER_VALIDATE_BOOLEAN);
            $query = $query->andFilterWhere(['approved_by_leed' => $approved_by_leed]);
        }

        if (isset($dataIn['approved_by_pm'])) {
            $approved_by_pm = filter_var($dataIn['approved_by_pm'], FILTER_VALIDATE_BOOLEAN);
            $query = $query->andFilterWhere(['approved_by_pm' => $approved_by_pm]);
        }

        if (isset($dataIn['approved_by_boss'])) {
            $approved_by_boss = filter_var($dataIn['approved_by_boss'], FILTER_VALIDATE_BOOLEAN);
            $query = $query->andFilterWhere(['approved_by_boss' => $approved_by_boss]);
        }


        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }


    public function checkAccess($action, $model = null, $params = [])
    {
        if ($action == "update") {
            $data = Yii::$app->getRequest()->getBodyParams();

            file_put_contents(Yii::getAlias('@webroot') . "/" . time() . "_update.json", json_encode($data));


            $canIChange = ($model->id == Yii::$app->user->id && $model->changed_by_chief !== true);
            $canLeedChange = ($model->chief_id == Yii::$app->user->id);
            $leed = Users::findOne($model->chief_id);
            $canPMChange = ($leed) ? ($leed->chief_id == Yii::$app->user->id) : false;
            $isBossRole = in_array(Yii::$app->user->id, Roles::globalChangeUsersRole());


            if (!($canIChange || $canLeedChange || $canPMChange || $isBossRole)) {
                throw new ForbiddenHttpException("Вы не можете редактировать заявки другого пользователя");
            } else {
                throw new UserException("Данный функционал еще не реализован");
            }
        }
        if ($action == "delete") {
           throw new UserException("Данный функционал еще не реализован");
        }
    }
}
