<?php

namespace app\controllers;


use app\models\Roles;
use Yii;
use app\models\Users;
use app\models\UsersSeach;
use yii\base\UserException;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\User;


class MemberController extends BaseActiveController
{
    public $modelClass = 'app\models\Users';

    public function actions()
    {
        $actions = parent::actions();
        $actions['update'] = [
            'class' => 'app\models\MemberUpdateAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
            'scenario' => $this->updateScenario,
        ];

        unset($actions['delete'],$actions['create']);//,$actions['update']);
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }


    public function prepareDataProvider () {

        $dataIn = Yii::$app->getRequest()->get();

        $query =  Users::find()->where(['inactive' => false]);
        // сюда добавить условие при котором отображаем всех если надо будет такое

        if (isset($dataIn['displayname'])) {
            $query = $query->andFilterWhere(['like', 'displayname', $dataIn['displayname']]);
        }

        if (isset($dataIn['ids'])) {
            $idArray = explode(",",$dataIn['ids']);
            $query = $query->andFilterWhere(['id' => $idArray]);
        }

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        if ($action == "update") {
            $data = Yii::$app->getRequest()->getBodyParams();

            $canIChange = ($model->id == Yii::$app->user->id && $model->changed_by_chief !== true);
            $canLeedChange = ($model->chief_id == Yii::$app->user->id);
            $leed = Users::findOne($model->chief_id);
            $canPMChange = ($leed) ? ($leed->chief_id == Yii::$app->user->id) : false;
            $isBossRole = in_array(Yii::$app->user->id, Roles::globalChangeUsersRole());


            if (!($canIChange || $canLeedChange || $canPMChange || $isBossRole)) {
                throw new ForbiddenHttpException("Вы не можете редактировать другого пользователя");
            }
        }
    }
}
