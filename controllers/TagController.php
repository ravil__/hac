<?php

namespace app\controllers;

use app\models\HeaderParamsAuth;
use app\models\Tag;
use Yii;
use app\models\Users;
use app\models\UsersSeach;
use yii\base\UserException;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\User;

class TagController extends BaseActiveController
{
    public $modelClass = 'app\models\Tag';

    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function prepareDataProvider () {

        $dataIn = Yii::$app->getRequest()->get();

        $query =  Tag::find();

        if (isset($dataIn['name'])) {
            $query = $query->andFilterWhere(['like', 'name', $dataIn['name']]);
        }

        if (isset($dataIn['ids'])) {
            $idArray = explode(",",$dataIn['ids']);
            $query = $query->andFilterWhere(['id' => $idArray]);
        }

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        if ($action == "delete"  ||  $action == "update") {
            throw new ForbiddenHttpException("пока  в системе никому нельзя удалять и модифицировать тэги");
        }
    }

}
