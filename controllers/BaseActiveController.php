<?php

namespace app\controllers;

use app\models\HeaderParamsAuth;
use Yii;
use yii\rest\ActiveController;
use yii\web\ForbiddenHttpException;


class BaseActiveController extends ActiveController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HeaderParamsAuth::className(),
        ];
        return $behaviors;
    }

    public function beforeAction ($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }

        \Yii::$app->response->headers->add("Access-Control-Allow-Origin", "*")->add("Access-Control-Allow-Methods", "*");
        return true;
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        // только если есть логин
        if (! isset(Yii::$app->user->identity)) {
            throw new ForbiddenHttpException("Ошибка авторизации");
        }

    }
}