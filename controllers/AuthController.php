<?php
/**
 * Created by PhpStorm.
 * User: ravil
 * Date: 5/19/16
 * Time: 6:19 PM
 */

namespace app\controllers;

use app\models\HeaderParamsAuth;
use yii\base\UserException;
use yii\rest\Controller;
use yii\web\UnauthorizedHttpException;



class AuthController extends Controller {

    public function beforeAction ($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }

        \Yii::$app->response->headers->add("Access-Control-Allow-Origin", "*")->add("Access-Control-Allow-Methods", "*");
        return true;
    }

    public function login() {

    }

    public function logout () {

    }

    public function checkSession() {

    }

}