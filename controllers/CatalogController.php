<?php
/**
 * Created by PhpStorm.
 * User: ravil
 * Date: 5/19/16
 * Time: 6:19 PM
 */

namespace app\controllers;


use app\models\Category;
use app\models\Place;
use app\models\Roles;
use app\models\Spec;

class CatalogController extends BaseAuthController {

    public function actionPlace () {
        $ref = new Place();
        return $ref->findAll();
    }

    public function actionSpec () {
        $ref = new Spec();
        return $ref->findAll();
    }

    public function actionRole () {
        $ref = new Roles();
        return $ref->findAll();
    }

    public function actionCategory () {
        $ref = new Category();
        return $ref->findAll();
    }
}