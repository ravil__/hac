<?php

namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\UsersSeach;
use yii\base\UserException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for Users model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersSeach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Users();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function getChif () {
        Yii::$app->chif;
    }

    public function actionLogin () {
        $inData = Yii::$app->request->getBodyParams();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        \Yii::$app->response->headers->add("Access-Control-Allow-Origin", "*")->add("Access-Control-Allow-Methods", "*");


        //iconv_set_encoding("input_encoding", "Windows-1251");

        $ds = ldap_connect("172.29.134.240");
        ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);

        if ($ds) {

            $r = ldap_bind($ds, "alt\\".$inData['username'], $inData['password']);

            $dn = "OU=HQ,DC=alt,DC=altarix,DC=ru";
            $sr = ldap_search($ds, $dn, "mailnickname=". $inData['username'], array('distinguishedname', 'department', 'displayname', 'cn', 'sn', 'title', 'company', 'mail', 'jpegphoto', 'thumbnailphoto', 'objectguid', 'objectsid'));

            $info = ldap_get_entries($ds, $sr);

            $out = array();
            for ($i = 0; $i < $info["count"]; $i++) {
                $out[] = array(
                    "cn" => (isset($info[$i]['cn'])) ?   $info[$i]['cn']["0"] : null,
                    "distinguishedname" => (isset($info[$i]['distinguishedname'])) ? $info[$i]['distinguishedname']["0"] : null,
                    "department" => (isset($info[$i]['department'])) ? $info[$i]['department']["0"] : null,
                    "displayname" => (isset($info[$i]['displayname'])) ? $info[$i]['displayname']["0"] : null,
                    "dn" => (isset($info[$i]['dn'])) ?  $info[$i]['dn'] : null,
                    "sn" => (isset($info[$i]['sn'])) ?  $info[$i]['sn']["0"] : null,
                    "title" => (isset($info[$i]['title'])) ?  $info[$i]['title']["0"] : null,
                    "mail" => (isset($info[$i]['mail'])) ?  $info[$i]['mail']["0"] : null,
                    "thumbnailphoto" => (isset($info[$i]['thumbnailphoto'])) ? base64_encode($info[$i]['thumbnailphoto']["0"]) : null,
                    "jpegphoto" => (isset($info[$i]['jpegphoto'])) ? base64_encode($info[$i]['jpegphoto']["0"]) : null,
                    "objectsid" => (isset($info[$i]['objectsid'])) ? base64_encode($info[$i]['objectsid']["0"]) : null,
                    "objectguid" => (isset($info[$i]['objectguid'])) ? base64_encode($info[$i]['objectguid']["0"]) : null,
                );
            }


            ldap_close($ds);

            if (count($out) != 1) {
                throw new UserException("Что-то пошло не так при авторизации");
            }

            $userData = $out[0];


            if (!$user = Users::find()->where(["objectguid" => $userData['objectguid']])->one()) {
                $user = new Users();
            }

            $user->cn = $userData['cn'];
            $user->objectsid = $userData['objectsid'];
            $user->mail = $userData['mail'];
            $user->dn = $userData['dn'];
            $user->sn = $userData['sn'];
            $user->displayname = $userData['displayname'];
            $user->title = $userData['title'];
            $user->objectguid = $userData['objectguid'];
            $user->auth_key = Yii::$app->security->generateRandomString();
            $user->isadmin = false;
            $user->thumbnailphoto = $userData['thumbnailphoto'];

            $result = $user->save();
            if (!$result) {
                throw new UserException(array_shift($user->getFirstErrors()));
            }else{
                return $user;
            }


        } else {
            throw new UserException ("Невозможно подключиться к серверу LDAP");
        }

    }


    public function actionUpdateUsers ($username, $password)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $ds = ldap_connect("172.29.141.130");
        ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
        if ($ds) {

            $r = ldap_bind($ds, "alt\\{$username}", $password);

            $dn = "OU=HQ,DC=alt,DC=altarix,DC=ru";
            $sr = ldap_search($ds, $dn, "(objectClass=user)", array( 'objectguid'));

            $info = ldap_get_entries($ds, $sr);

            $out = array();
            for ($i = 0; $i < $info["count"]; $i++) {
                $out[] = array(
                    "dn" => (isset($info[$i]['dn'])) ? iconv("Windows-1251", "UTF-8", $info[$i]['dn']) : null,
                    "objectguid" => (isset($info[$i]['objectguid'])) ? base64_encode($info[$i]['objectguid']["0"]) : null,
                );
            }

            ldap_close($ds);
            $result = null;
            $errors = [];
            foreach ($out as $userData) {

                if ($user = Users::find()->where(["objectguid" => $userData['objectguid']])->one()) {
                    $user->inactive = (strpos( $userData['dn'] , "Отключенные_пользователи") === false)? false : true;
                    $result = $user->save();

                    if (!$result) {
                        $errors[] = "ошибка сосхранения ".array_shift($user->getFirstErrors());
                    }
                }else{
                    $errors[] = "не нашлось юзера ".$userData['objectguid'];
                }
            }

            return ['Ошибки' => $errors];

        } else {
            throw new UserException ("Невозможно подключиться к серверу LDAP");
        }

    }


    public function actionLoadUsers ($username, $password)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $ds = ldap_connect("172.29.141.130");
        ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
        if ($ds) {

            $r = ldap_bind($ds, "alt\\{$username}", $password);

            $dn = "OU=HQ,DC=alt,DC=altarix,DC=ru";
            $sr = ldap_search($ds, $dn, "(objectClass=user)", array('distinguishedname', 'department', 'displayname', 'cn', 'sn', 'title', 'company', 'mail', 'jpegphoto', 'thumbnailphoto', 'objectguid', 'objectsid'));

            $info = ldap_get_entries($ds, $sr);

            $out = array();
            for ($i = 0; $i < $info["count"]; $i++) {
                $out[] = array(
                    "cn" => (isset($info[$i]['cn'])) ?   $info[$i]['cn']["0"] : null,
                    "distinguishedname" => (isset($info[$i]['distinguishedname'])) ? $info[$i]['distinguishedname']["0"] : null,
                    "department" => (isset($info[$i]['department'])) ? $info[$i]['department']["0"] : null,
                    "displayname" => (isset($info[$i]['displayname'])) ? $info[$i]['displayname']["0"] : null,
                    "dn" => (isset($info[$i]['dn'])) ?  $info[$i]['dn'] : null,
                    "sn" => (isset($info[$i]['sn'])) ?  $info[$i]['sn']["0"] : null,
                    "title" => (isset($info[$i]['title'])) ?  $info[$i]['title']["0"] : null,
                    "mail" => (isset($info[$i]['mail'])) ?  $info[$i]['mail']["0"] : null,
                    "thumbnailphoto" => (isset($info[$i]['thumbnailphoto'])) ? base64_encode($info[$i]['thumbnailphoto']["0"]) : null,
                    "jpegphoto" => (isset($info[$i]['jpegphoto'])) ? base64_encode($info[$i]['jpegphoto']["0"]) : null,
                    "objectsid" => (isset($info[$i]['objectsid'])) ? base64_encode($info[$i]['objectsid']["0"]) : null,
                    "objectguid" => (isset($info[$i]['objectguid'])) ? base64_encode($info[$i]['objectguid']["0"]) : null,
                );
            }

            ldap_close($ds);



            $result = null;
            $errors = [];
            foreach ($out as $userData) {

                $user = new Users();

                    $user->cn = $userData['cn'];
                    $user->objectsid = $userData['objectsid'];
                    $user->mail = $userData['mail'];
                    $user->dn = $userData['dn'];
                    $user->sn = $userData['sn'];
                    $user->displayname = $userData['displayname'];
                    $user->title = $userData['title'];
                    $user->objectguid = $userData['objectguid'];

                    $user->auth_key = Yii::$app->security->generateRandomString();
                    $user->isadmin = false;
                    $user->thumbnailphoto = $userData['thumbnailphoto'];
                    $user->inactive = (strpos( $userData['dn'] , "Отключенные_пользователи") === false)? false : true;
                    $result = $user->save();

                    if (!$result) {
                        $errors[] = "ошибка сосхранения ".json_encode($user->getFirstErrors());
                    }

            }

            return ['Ошибки' => $errors];

        } else {
            throw new UserException ("Невозможно подключиться к серверу LDAP");
        }

    }
}
